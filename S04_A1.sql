/*Find all artists that have the letter "D" in their name:*/
SELECT * FROM artists WHERE name LIKE '%D%';

/*Find all songs that have a length of less than 3 minutes and 50 seconds (3:50):*/
SELECT * FROM songs WHERE length < '00:03:50';

/*Join the "artists" and "albums" tables and find all albums that have the letter "A" in their name:*/
SELECT artists.name AS artist_name, albums.* 
FROM artists 
JOIN albums ON artists.id = albums.artist_id
WHERE album_title LIKE '%A%';

/*Join the "albums" and "songs" tables and only show the album name, song name, and song length:*/
SELECT albums.album_title, songs.song_name, songs.length 
FROM albums 
JOIN songs ON albums.id = songs.album_id;

/*Sort the albums in Z-A order and show only the first 4 records:*/
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

/*Join the "albums" and "songs" tables, sort albums from Z-A, and display the results:*/
SELECT albums.album_title, songs.song_name, songs.length 
FROM albums 
JOIN songs ON albums.id = songs.album_id
ORDER BY albums.album_title DESC;
